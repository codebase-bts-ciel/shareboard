# Application Web en PHP Objet avec architecture MVC

Objectif du site : partager au sein d’une communauté des liens vers des sites web (→ _shares_) que l’on juge intéressants.

Utilise le framework CSS [Bootstrap 4](https://getbootstrap.com/) pour le rendu des pages.

Fork du projet : [oop-php-mvc](https://github.com/elmerdotdev/oop-php-mvc)

## Fonctionnalités
* Ajout, Liste, Mise à jour, Suppression d'entrées
* Enregistrement utilisateur
* Connexion/Déconnexion utilisateur
* Métriques de popularité du site
* Hachage de mot de passe (chiffrement bcrypt)

Les vidéos suivantes illustrent les évolutions apportées au projet initial :

* Possibilité de préciser une catégorie pour le partage créé

![alt text](img/demo-domain.mp4 "Démo domaines")

* Métriques de popularité du site

![alt text](img/demo-insight.mp4 "Démo métriques")

## Installation

### Configuration du site

Modifier le fichier *config.php* pour spécifier le nom de la BDD, les données d'identification de la BDD, l'hôte, l'URL du site.

### Base de données

* Créer depuis phpMyAdmin 3 tables appelées*shares*, *users* et *domains* avec la structure suivante :

![alt text](img/dbshareboard-schema.svg "Modèle BDD")

* initialiser la table *domains* comme suit :

```
+----+-----------------------------------+
| id | description                       |
+----+-----------------------------------+
|  1 | Network design and engineering    |
|  2 | Network operation and maintenance |
|  3 | Data valorization / Cybersecurity |
|  4 | Other                             |
+----+-----------------------------------+
```

## UML

### Diagramme de classes

![alt text](img/class-diagram.svg "Diagramme de classes")

### Diagrammes de séquence

* Diagramme de séquence pour la page d'accueil :

![alt text](img/shareboard-seq-diagram-home.svg "Diagramme de séquence page d'accueil")

* Diagramme de séquence pour les partages (liste + ajout)

![alt text](img/shareboard-seq-diagram-shares.svg "Diagramme de séquence partages (liste + ajout)")