<div class="card col col-lg-5 mx-3 mx-lg-auto p-0">
	<div class="card-header">
		<h3 class="card-title">Share Something</h3>
	</div>
	<div class="card-body">
		<form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
			<div class="form-group">
				<label for="title">Share Title</label>
				<input type="text" name="title" class="form-control" id="title" required />
			</div>
			<div class="form-group">
				<label for="body">Body</label>
				<textarea name="body" class="form-control" id="body" required ></textarea>
			</div>
			<div class="form-group">
			<fieldset class="form-group row">
				<legend class="col-form-label col-sm-3 float-sm-left pt-0">Domain</legend>
				<div class="col-sm-7">
				<div class="form-check">
					<input class="form-check-input" type="radio" name="domain_id" id="domain01" value="1">
					<label class="form-check-label" for="domain01">Network design and engineering</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="domain_id" id="domain02" value="2">
					<label class="form-check-label" for="domain02">Network operation and maintenance</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="domain_id" id="domain03" value="3">
					<label class="form-check-label" for="domain03">Data valorization / Cybersecurity</label>
				</div>
				<div class="form-check">
					<input class="form-check-input" type="radio" name="domain_id" id="domainOther" value="4">
					<label class="form-check-label" for="domain03">Other</label>
				</div>
				</div>
			</fieldset>
			<div class="form-group">
				<label for="link">Link</label>
				<input type="text" name="link" class="form-control" id="link" placeholder="https://www.google.com" />
			</div>
			<input class="btn btn-primary" name="submit" type="submit" value="Submit" />
			<a class="btn btn-danger" href="<?php echo ROOT_PATH; ?>shares">Cancel</a>
		</form>
	</div>
</div>