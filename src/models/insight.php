<?php

class Data {
	public array $labels;
	public array $datasets;
}

class DataSet {
	public string $label;
	public array $data;
}

class InsightModel extends Model{
	public function __construct(){
		parent::__construct();

		$_SESSION['is_insights'] = true;
	}

	public function Index(){
		return;
	}

	public function show(){
		// Sanitize
		$get = filter_input_array(INPUT_GET, FILTER_SANITIZE_STRING);

		if($get['id'] != NULL || $get['id'] != ''){
			$dataset = new DataSet();
			$dataset->data = [];
			$data = new Data();
			$data->labels = [];

			switch($get['id']) {
				case 'd' :
					$dataset->label = "Daily";
					for($h=0; $h < 24; $h++) {
						$this->query('SELECT COUNT(*) FROM shares WHERE DAY(create_date) = DAY(NOW()) AND HOUR(create_date)=:h;');
						$this->bind(':h', $h);
						array_push($dataset->data, $this->countSet());
						
						array_push($data->labels, sprintf("%02dh", $h));
					}
					$data->datasets = [$dataset];
					break;

				case 'w' :
					$dataset->label = "Weekly";
					for($d=1; $d <= 7; $d++) {
						$this->query('SELECT COUNT(*) FROM shares WHERE WEEK(create_date) = WEEK(NOW()) AND DAY(create_date)=:d;');
						$this->bind(':d', $d);
						array_push($dataset->data, $this->countSet());
						
						array_push($data->labels, jddayofweek($d-1,2));
					}
					$data->datasets = [$dataset];
					break;

					case 'm' :
					$dataset->label = "Monthly";
					for($m=1; $m <= 12; $m++) {
						$this->query('SELECT COUNT(*) FROM shares WHERE YEAR(create_date) = YEAR(NOW()) AND MONTH(create_date)=:m;');
						$this->bind(':m', $m);
						array_push($dataset->data, $this->countSet());
						
						array_push($data->labels, jdmonthname(gregoriantojd($m, 1, 1), CAL_MONTH_JULIAN_SHORT));
					}
					$data->datasets = [$dataset];					
					break;
			}
			
			$jsonData = json_encode($data);
			
			return $jsonData;
		} else {
			header('Location: ' . ROOT_PATH . 'insights');
		}
	}

}

?>