<div class="col col-lg-8 mt-5 mx-3 mx-lg-auto text-center">
	<a class="btn btn-outline-primary" href="<?php echo ROOT_PATH; ?>insights/show/d" role="button">Daily</a>
	<a class="btn btn-outline-primary" href="<?php echo ROOT_PATH; ?>insights/show/w" role="button">Weekly</a>
	<a class="btn btn-outline-primary" href="<?php echo ROOT_PATH; ?>insights/show/m" role="button">Monthly</a>

	<div id="chart-wrapper">
		<canvas id="insights"></canvas>
	</div>

</div>

<script>
let ctx = document.querySelector("#insights");

let graph = new Chart(ctx, {
    type : 'line',
    data : <?php echo $viewmodel ?>,
	options: {
    	responsive: true
  	}
});
</script>