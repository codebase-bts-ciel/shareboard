<div class="col col-lg-8 mt-5 mx-3 mx-lg-auto text-center">
	<p class="lead">Get more insights into your website traffic by clicking on the buttons below.</p>
	<a class="btn btn-outline-primary" href="<?php echo ROOT_PATH; ?>insights/show/d" role="button">Daily</a>
	<a class="btn btn-outline-primary" href="<?php echo ROOT_PATH; ?>insights/show/w" role="button">Weekly</a>
	<a class="btn btn-outline-primary" href="<?php echo ROOT_PATH; ?>insights/show/m" role="button">Monthly</a>
</div>




